Dirty Phrasebook
================

Dirty Phrasebook is a joke translation app based upon Monty Python's Dirty Hungarian Phrasebook sketch, and was published to Google Play on 1st April 2015.

The operation of the app is documented in a [series of posts on the Styling Android blog](https://blog.stylingandroid.com/dirty-phrasebook-part-1).

I am immensely grateful to the following people who, most generously, provided their time and language skills:

* [Sebastiano Poggi](https://plus.google.com/+SebastianoPoggi) (Italian)
* [Zvonko Grujić](https://plus.google.com/+ZvonkoGrujić) (Croatian)
* [Conor O'Donnell](https://plus.google.com/116437815822251696235) (Gaelic)
* [Stefan Hoth](https://plus.google.com/+StefanHoth) (German)
* [Hans Petter Eide](https://plus.google.com/+HansPetterEide) (Norwegian)
* [Wiebe Elsinga](https://plus.google.com/+WiebeElsinga) (Dutch)
* [Imanol Pérez Iriarte](https://plus.google.com/+ImanolPérezIriarte) (Spanish)
* [Adam Graves](https://plus.google.com/112638338946187398427) (Malay)
* [Teo Ramone](https://plus.google.com/+teoramone) (Greek)
* [Mattias Isegran Bergander](https://plus.google.com/+MattiasIsegranBergander) (Swedish)
* [Morten Grouleff](https://plus.google.com/+MortenGrouleff) (Danish)
* [George Medve](https://plus.google.com/+GeorgeMedve) (Hungarian)
* [Anup Cowkur](https://plus.google.com/+AnupCowkur89) (Hindi)
* [Draško Sarić](https://plus.google.com/+DraskoSaric) (Serbian)
* [Polson Keeratibumrungpong](https://plus.google.com/+PolsonKeeratibumrungpong) (Thai)
* [Benoit Duffez](https://plus.google.com/+BenoitDuffez) (French)
* [Vasily Sochinsky](https://plus.google.com/+VasilySochinsky) (Russian)

Additional Translations
-----------------------

Additional translations are most welcome and periodic updates of the Dirty Phrasebook app will be made with new translations when appropriate. However, due to time constraints, 
new translations will only be accepted if they are submitted in an easy to integrate form. The process for adding translations is:

1. Fork this repo.
2. Edit res/values/phrases.xml and add a new string-array containing the nine strings translated to your chosen language ensuring that the order matches the English.
3. Name the string-array using the [ISO-639-1](http://en.wikipedia.org/wiki/ISO_639-1) code for your language.
4. Edit res/values/languages.xml and add the ISO-639-1 code to the *end* of the 'languages' array, and the human readable name of your chosen language to the *end* of the 'readable_languages' array. Please note that these arrays get sorted at runtime so the ordering here does not affect their position in the dropdown list, which is purely alphabetical.
5. Edit README.md (this file) and add your name and contact details to the list of translators.
6. Build and run the app and make sure that there are no errors.
7. Open a Pull Request and, if you have followed these steps carefully, it will be reviewed and merged.


