package com.stylingandroid.dirtyphrasebook;

public class DeveloperException extends RuntimeException {
    public DeveloperException(String s) {
        super(s);
    }
}
