package com.stylingandroid.dirtyphrasebook.tts;

import android.annotation.TargetApi;
import android.os.Build;
import android.speech.tts.TextToSpeech;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class LollipopTextToSpeech extends TextToSpeechCompat {
    protected LollipopTextToSpeech(TextToSpeech textToSpeech, VolumeController volumeController) {
        super(textToSpeech, volumeController);
    }

    @Override
    public int speak(CharSequence text, int queueMode, String utteranceId) {
        return getTextToSpeech().speak(text, queueMode, null, utteranceId);
    }
}
