package com.stylingandroid.dirtyphrasebook.tts;

import android.speech.tts.TextToSpeech;

import java.util.HashMap;
import java.util.Map;

class LegacyTextToSpeech extends TextToSpeechCompat {
    protected LegacyTextToSpeech(TextToSpeech textToSpeech, VolumeController volumeController) {
        super(textToSpeech, volumeController);
    }

    @Override
    @SuppressWarnings("deprecation")
    public int speak(CharSequence text, int queueMode, String utterenceId) {
        Map<String, String> params = new HashMap<>();
        if (utterenceId != null) {
            params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, utterenceId);
        }
        return getTextToSpeech().speak(text.toString(), queueMode, (HashMap<String, String>) params);
    }
}
