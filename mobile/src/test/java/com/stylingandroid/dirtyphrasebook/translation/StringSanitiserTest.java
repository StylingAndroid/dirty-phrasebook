package com.stylingandroid.dirtyphrasebook.translation;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class StringSanitiserTest {

    public static final String EXPECTED_OUTPUT = "hello world";

    @Test
    public void testNormaliseCase() {
        assertEquals("All capitals", EXPECTED_OUTPUT, StringSanitser.normaliseCase("HELLO WORLD"));
        assertEquals("All lowercase", EXPECTED_OUTPUT, StringSanitser.normaliseCase("hello world"));
        assertEquals("Camelcase", EXPECTED_OUTPUT, StringSanitser.normaliseCase("Hello World"));
    }

    @Test
    public void testRemoveNonLetters() {
        assertEquals("Exclamation", EXPECTED_OUTPUT, StringSanitser.removeNonLetters("hello world!"));
        assertEquals("Quotes", EXPECTED_OUTPUT, StringSanitser.removeNonLetters("\"hello world\""));
        assertEquals("Greek", "ελληνικά", StringSanitser.removeNonLetters("ελληνικά"));

    }

    @Test
    public void testRemoveDiacritics() {
        assertEquals("A ring", "hallo world", StringSanitser.removeDiacritics("hållo world"));
        assertEquals("E acute", EXPECTED_OUTPUT, StringSanitser.removeDiacritics("héllo world"));
        assertEquals("E grave", EXPECTED_OUTPUT, StringSanitser.removeDiacritics("hèllo world"));
        assertEquals("O dieresis", EXPECTED_OUTPUT, StringSanitser.removeDiacritics("hello wörld"));
    }

    @Test
    public void testRemoveSpaces() {
        assertEquals("Leading space", EXPECTED_OUTPUT, StringSanitser.sanitise(" hello world"));
        assertEquals("Trailing space", EXPECTED_OUTPUT, StringSanitser.sanitise("hello world "));
        assertEquals("Double space", EXPECTED_OUTPUT, StringSanitser.removeMultipleSpaces("hello  world"));
    }

    @Test
    public void testSanitise() {
        assertEquals("Capitals, accents, and punctuation", EXPECTED_OUTPUT, StringSanitser.sanitise(" Héllo      Wörld ! "));
    }
}
